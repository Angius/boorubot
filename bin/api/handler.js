"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
require("../embed");
var embed_1 = require("../embed");
var api;
(function (api) {
    var Handler = /** @class */ (function () {
        function Handler(engine) {
            this.engine = engine;
            this.apiUrl = engine.valueOf();
        }
        Handler.prototype.sendDerpi = function (query, msg, engine) {
            var q = query;
            axios_1.default.get(engine.toString() + 'search.json?q=' + q + '&random_image=true')
                .then(function (response) {
                console.log('Get random: ' + response.status);
                axios_1.default.get(engine.toString() + response.data.id + '.json')
                    .then(function (res) {
                    console.log('Get image: ' + res.status);
                    var d = res.data;
                    var e = new embed_1.Embed(d.id, 'https:' + d.image, d.score, engine.toString() + d.id, d.width, d.height, msg);
                    e.send();
                })
                    .catch(function (error) {
                    console.log(error);
                });
            })
                .catch(function (error) {
                console.log(error);
            });
        };
        Handler.prototype.sendGel = function (query, msg, engine) {
            var q = query;
            var id = api.Handler.getRandomInt(0, 99);
            console.log(engine.toString() + 'index.php?page=dapi&s=post&q=index&json=1&tags=' + q);
            axios_1.default.get(engine.toString() + 'index.php?page=dapi&s=post&q=index&json=1&tags=' + q)
                .then(function (response) {
                console.log('Get image: ' + response.status);
                var d = response.data[id];
                var e = new embed_1.Embed(d.id, engine.toString() + "/images" + d.directory + "/" + d.image, d.score, d.source, d.width, d.height, msg);
                console.log(engine.toString() + "/images/" + d.directory + "/" + d.image);
                e.send();
            })
                .catch(function (error) {
                console.log(error);
            });
        };
        Handler.prototype.sendDan = function (query, msg, engine) {
            var q = query;
            var id = api.Handler.getRandomInt(0, 99);
            console.log(engine.toString() + 'posts.json?tags=' + q + '&limit=100');
            axios_1.default.get(engine.toString() + 'posts.json?tags=' + q + '&limit=100')
                .then(function (response) {
                console.log('Get image: ' + response.status);
                var d = response.data[id];
                var e = new embed_1.Embed(d.id, 'https:' + d.file_url ? d.file_url : 'https://previews.123rf.com/images/aquir/aquir1705/aquir170505923/77698681-content-blocked-round-isolated-gold-badge.jpg', d.score, d.source, d.image_width, d.image_height, msg);
                e.send();
            })
                .catch(function (error) {
                console.log(error);
            });
        };
        Handler.prototype.sendE621 = function (query, msg, engine) {
            var q = query;
            var id = api.Handler.getRandomInt(0, 99);
            console.log(engine.toString() + 'post/index.json?tags=' + q + '&limit=100');
            axios_1.default.get(engine.toString() + 'post/index.json?tags=' + q + '&limit=100')
                .then(function (response) {
                console.log('Get image: ' + response.status);
                var d = response.data[id];
                var e = new embed_1.Embed(d.id, d.file_url, d.score, d.source, d.width, d.height, msg);
                e.send();
            })
                .catch(function (error) {
                console.log(error);
            });
        };
        Handler.getRandomInt = function (min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        return Handler;
    }());
    api.Handler = Handler;
    var Engine;
    (function (Engine) {
        Engine["Gelbooru"] = "https://gelbooru.org/";
        Engine["Danbooru"] = "https://danbooru.donmai.us/";
        Engine["Derpibooru"] = "https://derpibooru.org/";
        Engine["Rule34"] = "https://rule34.xxx/";
        Engine["E621"] = "https://e621.net/";
    })(Engine = api.Engine || (api.Engine = {}));
})(api = exports.api || (exports.api = {}));
