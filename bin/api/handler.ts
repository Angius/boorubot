import axios from "axios";
import * as Discord from "discord.js";

import "../embed"
import {Embed} from "../embed";

export namespace api {
    export class Handler {
        private engine: Engine;
        public apiUrl: string;

        constructor(engine: Engine) {
            this.engine = engine;
            this.apiUrl = engine.valueOf();
        }

        sendDerpi(query: string, msg: Discord.Message, engine: Engine) {
            let q = query;
            axios.get(engine.toString() + 'search.json?q=' + q + '&random_image=true')
                .then(response => {
                    console.log('Get random: ' + response.status);
                    axios.get(engine.toString() + response.data.id + '.json')
                        .then(res => {
                            console.log('Get image: ' + res.status);
                            let d = res.data;
                            let e = new Embed(d.id, 'https:' + d.image, d.score, engine.toString() + d.id, d.width, d.height, msg);
                            e.send();
                        })
                        .catch(error => {
                            console.log(error);
                        });
                })
                .catch(error => {
                    console.log(error);
                });
        }

        sendGel(query: string, msg: Discord.Message, engine: Engine) {
            let q = query;
            let id = api.Handler.getRandomInt(0, 99);
            console.log(engine.toString() + 'index.php?page=dapi&s=post&q=index&json=1&tags=' + q);
            axios.get(engine.toString() + 'index.php?page=dapi&s=post&q=index&json=1&tags=' + q)
                .then(response => {
                    console.log('Get image: ' + response.status);
                    let d = response.data[id];
                    let e = new Embed(d.id, `${engine.toString()}/images${d.directory}/${d.image}`, d.score, d.source, d.width, d.height, msg);
                    console.log(`${engine.toString()}/images/${d.directory}/${d.image}`);
                    e.send();
                })
                .catch(error => {
                    console.log(error);
                });
        }

        sendDan(query: string, msg: Discord.Message, engine: Engine) {
            let q = query;
            let id = api.Handler.getRandomInt(0, 99);
            console.log(engine.toString() + 'posts.json?tags=' + q + '&limit=100');
            axios.get(engine.toString() + 'posts.json?tags=' + q + '&limit=100')
                .then(response => {
                    console.log('Get image: ' + response.status);
                    let d = response.data[id];
                    let e = new Embed(d.id, 'https:' + d.file_url ? d.file_url : 'https://previews.123rf.com/images/aquir/aquir1705/aquir170505923/77698681-content-blocked-round-isolated-gold-badge.jpg', d.score, d.source, d.image_width, d.image_height, msg);
                    e.send();
                })
                .catch(error => {
                    console.log(error);
                });
        }

        sendE621(query: string, msg: Discord.Message, engine: Engine) {
            let q = query;
            let id = api.Handler.getRandomInt(0, 99);
            console.log(engine.toString() + 'post/index.json?tags=' + q + '&limit=100');
            axios.get(engine.toString() + 'post/index.json?tags=' + q + '&limit=100')
                .then(response => {
                    console.log('Get image: ' + response.status);
                    let d = response.data[id];
                    let e = new Embed(d.id, d.file_url, d.score, d.source, d.width, d.height, msg);
                    e.send();
                })
                .catch(error => {
                    console.log(error);
                });
        }

        private static getRandomInt(min: number, max: number): number {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }

    export enum Engine {
        Gelbooru = 'https://gelbooru.org/',
        Danbooru = 'https://danbooru.donmai.us/',
        Derpibooru = 'https://derpibooru.org/',
        Rule34 = 'https://rule34.xxx/',
        E621 = 'https://e621.net/',
    }
}
