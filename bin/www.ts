import * as Discord from "discord.js";

import {api} from "./api/handler";
import Handler = api.Handler;
import Engine = api.Engine;

const prefix: string = 'bb.';
const client: Discord.Client = new Discord.Client();

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async msg => {
    if (!msg.content.startsWith(prefix) || msg.author.bot) return;

    const args = msg.content.slice(prefix.length).split(' ');
    const command = args.shift().toLowerCase();

    let h: Handler;
    switch (command) {
        case 'gel':
            h = new Handler(api.Engine.Gelbooru);
            msg.channel.send(`There you go, <@${msg.member.id.toString()}>`);
            h.sendGel(args.length > 0 ? args.join(' ') : 'chrysalis', msg, Engine.Gelbooru);
            break;
        case 'dan':
            h = new Handler(api.Engine.Derpibooru);
            msg.channel.send(`There you go, <@${msg.member.id.toString()}>`);
            h.sendDan(args.length > 0 ? args.join(' ') : 'chrysalis', msg, Engine.Derpibooru);
            break;
        case 'derpi':
            h = new Handler(api.Engine.Derpibooru);
            msg.channel.send(`There you go, <@${msg.member.id.toString()}>`);
            h.sendDerpi(args.length > 0 ? args.join(' ') : 'chrysalis', msg, Engine.Danbooru);
            break;
        case 'e621':
            h = new Handler(api.Engine.E621);
            msg.channel.send(`There you go, <@${msg.member.id.toString()}>`);
            h.sendE621(args.length > 0 ? args.join(' ') : 'chrysalis', msg, Engine.E621);
            break;
        case 'r34':
            h = new Handler(api.Engine.Rule34);
            msg.channel.send(`There you go, <@${msg.member.id.toString()}>`);
            h.sendGel(args.length > 0 ? args.join(' ') : 'chrysalis', msg, Engine.Rule34);
            break;
        default:
            msg.reply("Didn't quite get that...")
    }


});

client.login(process.env.KEY);