"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Discord = require("discord.js");
var Embed = /** @class */ (function () {
    function Embed(id, image, score, source, width, height, message) {
        this.id = id;
        this.image = image;
        this.score = score;
        this.source = source;
        this.width = width;
        this.height = height;
        this.message = message;
    }
    Embed.prototype.send = function () {
        var embed = new Discord.RichEmbed()
            .setColor('#0099ff')
            .setTitle("**Source:** " + this.source)
            // .setURL(this.source)
            // .setAuthor('Some name', 'https://i.imgur.com/wSTFkRM.png', 'https://discord.js.org')
            .setDescription("**ID:** " + this.id + " | **Score:** " + this.score + " | **Resolution:** " + this.width + "x" + this.height)
            // .setThumbnail('https://i.imgur.com/wSTFkRM.png')
            // .addField('Regular field title', 'Some value here')
            // .addBlankField()
            // .addField('Inline field title', 'Some value here', true)
            // .addField('Inline field title', 'Some value here', true)
            // .addField('Inline field title', 'Some value here', true)
            .setImage(this.image)
            .setTimestamp();
        this.message.channel.send(embed);
    };
    return Embed;
}());
exports.Embed = Embed;
