import * as Discord from "discord.js";

export class Embed {
    private id: number;
    private image: string;
    private score: number;
    private source: string;
    private width: number;
    private height: number;

    private message: Discord.Message;


    constructor(id: number, image: string, score: number, source: string, width: number, height: number, message: Discord.Message) {
        this.id = id;
        this.image = image;
        this.score = score;
        this.source = source;
        this.width = width;
        this.height = height;
        this.message = message;
    }

    send() {
        const embed = new Discord.RichEmbed()
            .setColor('#0099ff')
            .setTitle(`**Source:** ${this.source}`)
            // .setURL(this.source)
            // .setAuthor('Some name', 'https://i.imgur.com/wSTFkRM.png', 'https://discord.js.org')
            .setDescription(`**ID:** ${this.id} | **Score:** ${this.score} | **Resolution:** ${this.width}x${this.height}`)
            // .setThumbnail('https://i.imgur.com/wSTFkRM.png')
            // .addField('Regular field title', 'Some value here')
            // .addBlankField()
            // .addField('Inline field title', 'Some value here', true)
            // .addField('Inline field title', 'Some value here', true)
            // .addField('Inline field title', 'Some value here', true)
            .setImage(this.image)
            .setTimestamp()
            // .setFooter(`**Source:** [${this.source}](${this.source})`, 'https://i.imgur.com/wSTFkRM.png')
        ;

        this.message.channel.send(embed);
    }
}